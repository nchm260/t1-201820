package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations 
{

	public int getMin(IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( min > value)
				{
					min = value;
				}
			}

		}
		return min; 
	}

	public int darPrimero (IntegersBag bag)
	{
		Iterator<Integer> iter = bag.getIterator();
		return iter.next();
	}
	public int darUltimo (IntegersBag bag)
	{
		Iterator<Integer> it = bag.getIterator();
		int ult = 0;
		while (it.hasNext())
		{
			ult = it.next();
		}
		return ult;
	}

	public double computeMean(IntegersBag bag)
	{
		double mean = 0;
		int length = 0;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag)
	{
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( max < value)
				{
					max = value;
				}
			}

		}
		return max;
	}
}
